﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using CMPS383P1.Models;

namespace CMPS383P1.Migrations
{
    [DbContext(typeof(CMPSContext))]
    [Migration("20170205225321_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CMPS383P1.Models.Inventory", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreatedByUserID");

                    b.Property<string>("Name");

                    b.Property<int>("Quantity");

                    b.HasKey("ID");

                    b.HasIndex("CreatedByUserID");

                    b.ToTable("InventoryItems");
                });

            modelBuilder.Entity("CMPS383P1.Models.User", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Password");

                    b.Property<string>("UserName")
                        .IsRequired();

                    b.HasKey("ID");

                    b.HasAlternateKey("UserName")
                        .HasName("AlternateKey_UserName");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("CMPS383P1.Models.Inventory", b =>
                {
                    b.HasOne("CMPS383P1.Models.User", "CreatedByUser")
                        .WithMany("InventoryItems")
                        .HasForeignKey("CreatedByUserID")
                        .HasConstraintName("FK_Inv_User")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
