﻿using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace CMPS383P1.Models
{
    public static class DBInit
    {
        public static void Initialize(CMPSContext context)
        {
            context.Database.EnsureCreated();
            if (context.Users.Any())
            {
                return;   // DB has been seeded
            }
            string Password1 = CryptoHelper.Crypto.HashPassword("selu2017");
            string Password2 = CryptoHelper.Crypto.HashPassword("Password");
            string Password3 = CryptoHelper.Crypto.HashPassword("Testing");
            var users = new User[]
            {
                new User {UserName = "admin", FirstName = "CMPS", LastName = "383", Password = Password1 },
                new User {UserName = "CB", FirstName = "Chad", LastName = "Bender", Password = Password2 },
                new User {UserName = "SarahF", FirstName = "Sarah", LastName = "Flores", Password = Password3 }
            };
            foreach (User s in users)
            {
                context.Users.Add(s);
            }
            context.SaveChanges();
            var InventoryItems = new Inventory[]
            {
                new Inventory {CreatedByUserID =1, Name="Potato", Quantity=5 },
                new Inventory {CreatedByUserID =1, Name="Banana", Quantity=15 },
                new Inventory {CreatedByUserID =1, Name="Apple", Quantity=10 },
                new Inventory {CreatedByUserID =1, Name="Orange", Quantity=20 }
            };
            foreach (Inventory c in InventoryItems)
            {
                context.InventoryItems.Add(c);
            }
            context.SaveChanges();
        }
    }
}
