﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMPS383P1.Models
{
    public class CMPSContext : DbContext
    {
        public CMPSContext(DbContextOptions<CMPSContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasAlternateKey(c => c.UserName)
                .HasName("AlternateKey_UserName");
            modelBuilder.Entity<Inventory>()
                .HasOne(i => i.CreatedByUser)
                .WithMany(u => u.InventoryItems)
                .HasForeignKey(i => i.CreatedByUserID)
                .HasConstraintName("FK_Inv_User");

        }

        public DbSet<Inventory> InventoryItems { get; set; }
    }
    public class User
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public ICollection<Inventory> InventoryItems { get; set; }
    }

    public class Inventory
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }

        public int CreatedByUserID { get; set; }
        public User CreatedByUser { get; set; }
    }
}
