# Group 5 |TL;DR Phase 1 Requirements #

### Important Information ###
1. Commit final project before __Monday night, February 6th (include presentation materials)__.
2. Presenting at __random__ on __Tuesday February 7th.
3. Each group has __15 minutes__ to present and take questions.
4. No more than __3 minutes__ for Powerpoint.
5. Prepare to show your work if asked.

### Description ###
* Creating an inventory tracking application.
* Public page (Front-end), login form to authenticate and sign in to portal.
* Admin side (Back-end), administrators should be able to add, edit, delete users and inventory items.
* REST endpoint?? allows users to make a __POST__ request and __"purchase items"__ and a __GET__ request that will allow them to see current levels.
* Users should be able to go to public page that will display all current inventory levels for all items.

### Technology Needed ###
[Visual Studio 2015](https://www.visualstudio.com/downloads/)  
[Microsft SQL server 2016](https://www.microsoft.com/en-us/sql-server/sql-server-downloads)  
[ASP.net Core](https://www.microsoft.com/net/download)  
* Entity Framework Core using the Code First method  
* Bitbucket  

#### Entities 
    * User
        * Id
        * Username
        * First Name
        * Last Name
        * Password
    * Inventory item
        * Id
        * CreatedByUserId ?!
        * Name
        * Quantity
        
### Technical Requirements ###

* Generate and seed database using Entity Framwork, Code First migrations.
    * When pulling project down to present, __Envoc__ will run the update databse command to create our database - __it cannot exist beforehand__.
    * Be sure to __seed__ an __"admin"__ user with the password __"selu2017"__.
* __Not__ to use the __"default"__ ASP.NET identity.
* __USERNAME__ must be unique and validated against the database.
* __PASSWORD__ must be hashed and verified against the stored hashed password and username in database.
* Authenticate users using [cookie middleware](https://docs.microsoft.com/en-us/aspnet/core/security/authentication/cookie).
* __Only__ the public login section, the inventory level viwing page, and the purchasing endpoints may be accessible when not logged in using
[authorize attribute](https://docs.microsoft.com/en-us/aspnet/core/security/authorization/simple).
* No Ids, passwords, or other "unfriendly" types of data that are irrelevant to the users of the site should appear on any web pages.

### User Stories ###
* __Administrator__ 
    * Login to access the __back-end management portal__ using user's credential.
    * Able to list, add, edit, delete __users__ from the system.
    * Able to list, add, edit, delete __inventory__ from the system.
* __User__
    * Able to go to a public page __without__ login in to see current inventory levels.
    * Submit a __GET request__ to an inventory endpoint to retrieve inventory levels from the system.
    * Execute a __POST request__ to an inventory endpoint to purchase an item(and decrease the inventory level).
### READING MATERIAL
[Code first method](https://msdn.microsoft.com/en-us/library/jj591621(v=vs.113).aspx)  
[ASP.net Identity](https://www.asp.net/identity/overview/getting-started/introduction-to-aspnet-identity)